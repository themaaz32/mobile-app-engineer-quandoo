import 'dart:io';

String getMockData(String fileName) => File("test/mock/json/$fileName").readAsStringSync();