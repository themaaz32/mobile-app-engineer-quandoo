import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:quandoo/core/error/exceptions.dart';
import 'package:quandoo/core/models/api_response_model.dart';
import 'package:quandoo/features/merchant/data/data_sources/merchant_remote_data_source.dart';
import 'package:quandoo/features/merchant/data/model/merchant_model.dart';
import 'package:quandoo/features/merchant/data/model/paginated_merchant_list_model.dart';
import 'package:quandoo/features/merchant/data/repository_implementation/merchant_repository_implementation.dart';

import '../../../../mock/mock_data_reader.dart';

class MockMerchantRemoteDataSource extends Mock
    implements MerchantRemoteDataSource {}

void main() {
  late MerchantRepositoryImplementation merchantRepositoryImplementation;
  late MockMerchantRemoteDataSource mockMerchantRemoteDataSource;
  late PaginatedMerchantListModel mockPaginatedMerchantListModel;
  late MerchantModel mockMerchantModel;
  late ApiResponse mockApiResponseWhenListGotten;
  late ApiResponse mockApiResponseWhenListException;
  late ApiResponse mockApiResponseWhenDetailGotten;
  late ApiResponse mockApiResponseWhenDetailException;
  const merchantId = 1;

  setUp(() {
    mockMerchantRemoteDataSource = MockMerchantRemoteDataSource();
    merchantRepositoryImplementation =
        MerchantRepositoryImplementation(mockMerchantRemoteDataSource);
    mockPaginatedMerchantListModel = PaginatedMerchantListModel.fromJson(
      jsonDecode(
        getMockData("paginated_merchant_list_mock_data.json"),
      ),
    );
    mockMerchantModel = MerchantModel.fromJson(
      jsonDecode(
        getMockData("merchant_mock_data.json"),
      ),
    );
    mockApiResponseWhenListGotten = ApiResponse<PaginatedMerchantListModel>(
      isSuccess: true,
      data: mockPaginatedMerchantListModel,
    );

    mockApiResponseWhenListException =
        const ApiResponse<PaginatedMerchantListModel>(
      isSuccess: false,
      error: ServerException(),
    );

    mockApiResponseWhenDetailException = const ApiResponse<MerchantModel>(
      isSuccess: false,
      error: ServerException(),
    );

    mockApiResponseWhenDetailGotten = ApiResponse<MerchantModel>(
      isSuccess: true,
      data: mockMerchantModel,
    );
  });

  group("getAllMerchants Test", () {
    test(
        "should get the valid response list "
        "of all merchants and true status when"
        "getAllMerchants gets called normally", () async {
      //arrange
      when(() => mockMerchantRemoteDataSource.getAllMerchants())
          .thenAnswer((_) async => mockPaginatedMerchantListModel);
      //act
      final response = await merchantRepositoryImplementation.getAllMerchants();
      //assert
      expect(response, mockApiResponseWhenListGotten);
    });
  });

  test(
      "should get the invalid response list "
      "with false status and error when "
      "getAllMerchants throw Exception", () async {
    //arrange
    when(() => mockMerchantRemoteDataSource.getAllMerchants()).thenThrow(
      const ServerException(),
    );
    //act
    final response = await merchantRepositoryImplementation.getAllMerchants();
    //assert
    expect(response, mockApiResponseWhenListException);
  });

  group("getMerchantDetail Test", () {
    test(
        "should get the valid response with "
        "merchant object and true status when"
        "getMerchantDetail gets called normally", () async {
      //arrange
      when(() => mockMerchantRemoteDataSource.getMerchantDetail(merchantId))
          .thenAnswer((_) async => mockMerchantModel);
      //act
      final response =
          await merchantRepositoryImplementation.getMerchantDetail(merchantId);
      //assert
      expect(response, mockApiResponseWhenDetailGotten);
    });
  });

  test(
      "should get the invalid response list "
      "with false status and error when "
      "getMerchantDetail throw Exception", () async {
    //arrange
    when(() => mockMerchantRemoteDataSource.getMerchantDetail(merchantId))
        .thenThrow(
      const ServerException(),
    );
    //act
    final response =
        await merchantRepositoryImplementation.getMerchantDetail(merchantId);
    //assert
    expect(response, mockApiResponseWhenDetailException);
  });
}
