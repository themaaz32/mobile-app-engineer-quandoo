import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:quandoo/features/merchant/data/model/timing_model.dart';
import '../../../../mock/mock_data_reader.dart';

void main(){

  const TimingModel timing = TimingModel(
    start: "17:30:00",
    end: "22:30:00",
  );

  // test('should be a sub type of Image Entity', (){
  //   //verify
  //   expect(tag, isA<Tag>);
  // });

  group("fromJson", (){
    test('should return a valid timing model', (){
      //arrange
      final mockData = getMockData("timing_mock_data.json");
      final json = jsonDecode(mockData);
      //act
      final timingFromJson = TimingModel.fromJson(json);
      //assert
      expect(timingFromJson, timing);
    });

    test('should return a valid timing json', (){
      //arrange
      final mockData = getMockData("timing_mock_data.json");
      final json = jsonDecode(mockData);
      //act
      final toJson = timing.toJson();
      //assert
      expect(toJson, json);
    });
  });

}