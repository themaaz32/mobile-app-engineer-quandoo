import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:quandoo/features/merchant/data/model/image_model.dart';
import '../../../../mock/mock_data_reader.dart';

void main(){

  const ImageModel image = ImageModel("https://someurl.com");

  // test('should be a sub type of Image Entity', (){
  //   //verify
  //   expect(image, isA<Image>);
  // });
  
  group("fromJson", (){
    test('should return a valid image model', (){
      //arrange
      final mockData = getMockData("image_mock_data.json");
      final json = jsonDecode(mockData);
      //act
      final imageFromJson = ImageModel.fromJson(json);
      //assert
      expect(imageFromJson, image);
    });

    test('should return a valid image json', (){
      //arrange
      final mockData = getMockData("image_mock_data.json");
      final json = jsonDecode(mockData);
      //act
      final toJson = image.toJson();
      //assert
      expect(toJson, json);
    });
  });

}