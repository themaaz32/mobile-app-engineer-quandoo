import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:quandoo/features/merchant/data/model/tag_group_model.dart';
import 'package:quandoo/features/merchant/data/model/tag_model.dart';
import '../../../../mock/mock_data_reader.dart';

void main(){

  const TagGroupModel tagGroup = TagGroupModel(
    "PRICE",
    [
      TagModel(
        id: "296929a4-324e-476b-b076-2f9c061d4756",
        name: "price average",
      ),
    ]
  );

  // test('should be a sub type of Image Entity', (){
  //   //verify
  //   expect(tag, isA<Tag>);
  // });

  group("fromJson", (){
    test('should return a valid tag group model', (){
      //arrange
      final mockData = getMockData("tag_group_mock_data.json");
      final json = jsonDecode(mockData);
      //act
      final tagGroupFromJson = TagGroupModel.fromJson(json);
      //assert
      expect(tagGroupFromJson, tagGroup);
    });

    test('should return a valid tag group json', (){
      //arrange
      final mockData = getMockData("tag_group_mock_data.json");
      final json = jsonDecode(mockData);
      //act
      final toJson = tagGroup.toJson();
      //assert
      expect(toJson, json);
    });
  });

}