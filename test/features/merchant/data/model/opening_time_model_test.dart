import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:quandoo/features/merchant/data/model/opening_time_model.dart';
import 'package:quandoo/features/merchant/data/model/timing_model.dart';
import '../../../../mock/mock_data_reader.dart';

void main() {
  const OpeningTimeModel openingTimeModel = OpeningTimeModel(
      {
        "standardOpeningTimes": {
          "WEDNESDAY": [
            TimingModel(
              start: "17:30:00",
              end: "22:30:00",
            ),
          ]
        }
      }
  );

  // test('should be a sub type of Image Entity', (){
  //   //verify
  //   expect(image, isA<Image>);
  // });

  group("fromJson", () {
    test('should return a valid opening time model', () {
      //arrange
      final mockData = getMockData("opening_timing_mock_data.json");
      final json = jsonDecode(mockData);
      //act
      final locationFromJson = OpeningTimeModel.fromJson(json);
      //assert
      expect(locationFromJson, openingTimeModel);
    });

    test('should return a valid opening time json', () {
      //arrange
      final mockData = getMockData("opening_timing_mock_data.json");
      final json = jsonDecode(mockData);
      //act
      final toJson = openingTimeModel.toJson();
      //assert
      expect(toJson, json);
    });
  });
}
