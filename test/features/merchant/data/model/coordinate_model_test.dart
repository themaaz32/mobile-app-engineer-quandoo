import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:quandoo/features/merchant/data/model/coordinate_model.dart';
import '../../../../mock/mock_data_reader.dart';

void main() {
  const CoordinateModel coordinate = CoordinateModel(
    longitude: 4.890594,
    latitude: 52.36635,
  );

  // test('should be a sub type of Image Entity', (){
  //   //verify
  //   expect(image, isA<Image>);
  // });

  group("fromJson", () {
    test('should return a valid coordinate model', () {
      //arrange
      final mockData = getMockData("coordinate_mock_data.json");
      final json = jsonDecode(mockData);
      //act
      final coordinateFromJson = CoordinateModel.fromJson(json);
      //assert
      expect(coordinateFromJson, coordinate);
    });

    test('should return a valid coordinate json', () {
      //arrange
      final mockData = getMockData("coordinate_mock_data.json");
      final json = jsonDecode(mockData);
      //act
      final toJson = coordinate.toJson();
      //assert
      expect(toJson, json);
    });
  });
}
