import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:quandoo/features/merchant/data/model/address_model.dart';
import 'package:quandoo/features/merchant/data/model/coordinate_model.dart';
import 'package:quandoo/features/merchant/data/model/location_model.dart';
import '../../../../mock/mock_data_reader.dart';

void main() {
  const LocationModel location = LocationModel(
    AddressModel(
      street: "Reguliersdwarsstraat",
      number: "30",
      zipCode: "1017 BM",
      city: "Amsterdam",
      country: "NLD",
      district: "Centrum",
    ),
    CoordinateModel(
      longitude: 4.890594,
      latitude: 52.36635,
    ),
  );

  // test('should be a sub type of Image Entity', (){
  //   //verify
  //   expect(image, isA<Image>);
  // });

  group("fromJson", () {
    test('should return a valid location model', () {
      //arrange
      final mockData = getMockData("location_mock_data.json");
      final json = jsonDecode(mockData);
      //act
      final locationFromJson = LocationModel.fromJson(json);
      //assert
      expect(locationFromJson, location);
    });

    test('should return a valid location json', () {
      //arrange
      final mockData = getMockData("location_mock_data.json");
      final json = jsonDecode(mockData);
      //act
      final toJson = location.toJson();
      //assert
      expect(toJson, json);
    });
  });
}
