import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:quandoo/features/merchant/data/model/address_model.dart';
import '../../../../mock/mock_data_reader.dart';

void main() {
  const AddressModel address = AddressModel(
        street: "Reguliersdwarsstraat",
        number: "30",
        zipCode: "1017 BM",
        city: "Amsterdam",
        country: "NLD",
        district: "Centrum",
  );

  const AddressModel emptyAddress = AddressModel();

  // test('should be a sub type of Image Entity', (){
  //   //verify
  //   expect(image, isA<Image>);
  // });

  group("fromJson", () {
    test('should return a valid address model when address is not empty', () {
      //arrange
      final mockData = getMockData("address_mock_data.json");
      final json = jsonDecode(mockData);
      //act
      final addressFromJson = AddressModel.fromJson(json);
      //assert
      expect(addressFromJson, address);
    });

    test('should return a valid address model when address is empty', () {
      //arrange
      final json = jsonDecode("{}");
      //act
      final addressFromJson = AddressModel.fromJson(json);
      //assert
      expect(addressFromJson, emptyAddress);
    });

    test('should return a valid address json', () {
      //arrange
      final mockData = getMockData("address_mock_data.json");
      final json = jsonDecode(mockData);
      //act
      final toJson = address.toJson();
      //assert
      expect(toJson, json);
    });
  });
}
