import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:quandoo/features/merchant/data/model/address_model.dart';
import 'package:quandoo/features/merchant/data/model/coordinate_model.dart';
import 'package:quandoo/features/merchant/data/model/image_model.dart';
import 'package:quandoo/features/merchant/data/model/location_model.dart';
import 'package:quandoo/features/merchant/data/model/merchant_model.dart';
import 'package:quandoo/features/merchant/data/model/opening_time_model.dart';
import 'package:quandoo/features/merchant/data/model/paginated_merchant_list_model.dart';
import 'package:quandoo/features/merchant/data/model/tag_group_model.dart';
import 'package:quandoo/features/merchant/data/model/tag_model.dart';
import 'package:quandoo/features/merchant/data/model/timing_model.dart';
import '../../../../mock/mock_data_reader.dart';

void main() {
  const MerchantModel merchant = MerchantModel(
    14918,
    "Restaurant Dynasty",
    "+31206268400",
    "EUR",
    "nl_NL",
    "Europe/Amsterdam",
    LocationModel(
      AddressModel(
        street: "Reguliersdwarsstraat",
        number: "30",
        zipCode: "1017 BM",
        city: "Amsterdam",
        country: "NLD",
        district: "Centrum",
      ),
      CoordinateModel(
        longitude: 4.890594,
        latitude: 52.36635,
      ),
    ),
    [
      TagGroupModel("PRICE", [
        TagModel(
          id: "296929a4-324e-476b-b076-2f9c061d4756",
          name: "price average",
        ),
      ]),
    ],
    "5.3",
    [
      ImageModel("https://someurl.com"),
    ],
    true,
    OpeningTimeModel({
      "standardOpeningTimes": {
        "WEDNESDAY": [
          TimingModel(
            start: "17:30:00",
            end: "22:30:00",
          ),
        ]
      }
    }),
    false,
  );
  const PaginatedMerchantListModel paginatedMerchantList =
      PaginatedMerchantListModel(
    [merchant,],
    2,
    0,
    100,
  );

  group("fromJson", () {
    test('should return a valid paginated merchant list model', () {
      //arrange
      final mockData = getMockData("paginated_merchant_list_mock_data.json");
      final json = jsonDecode(mockData);
      //act
      final paginatedMerchantListFromJson = PaginatedMerchantListModel.fromJson(json);
      //assert
      expect(paginatedMerchantListFromJson, paginatedMerchantList);
    });

    test('should return a valid paginated merchant list json', () {
      //arrange
      final mockData = getMockData("paginated_merchant_list_mock_data.json");
      final json = jsonDecode(mockData);
      //act
      final toJson = paginatedMerchantList.toJson();
      //assert
      expect(toJson, json);
    });
  });
}
