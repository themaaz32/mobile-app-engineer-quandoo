import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:quandoo/features/merchant/data/model/tag_model.dart';
import '../../../../mock/mock_data_reader.dart';

void main(){

  const TagModel tag = TagModel(
    id: "296929a4-324e-476b-b076-2f9c061d4756",
    name: "price average",
  );

  // test('should be a sub type of Image Entity', (){
  //   //verify
  //   expect(tag, isA<Tag>);
  // });

  group("fromJson", (){
    test('should return a valid tag model', (){
      //arrange
      final mockData = getMockData("tag_mock_data.json");
      final json = jsonDecode(mockData);
      //act
      final tagFromJson = TagModel.fromJson(json);
      //assert
      expect(tagFromJson, tag);
    });

    test('should return a valid tag json', (){
      //arrange
      final mockData = getMockData("tag_mock_data.json");
      final json = jsonDecode(mockData);
      //act
      final toJson = tag.toJson();
      //assert
      expect(toJson, json);
    });
  });

}