import 'dart:convert';

import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:quandoo/core/error/exceptions.dart';
import 'package:quandoo/core/models/all_merchant_usecase_param.dart';
import 'package:quandoo/core/models/api_response_model.dart';
import 'package:quandoo/features/merchant/data/model/merchant_model.dart';
import 'package:quandoo/features/merchant/data/model/paginated_merchant_list_model.dart';
import 'package:quandoo/features/merchant/domain/entity/merchant_entity.dart';
import 'package:quandoo/features/merchant/domain/entity/paginated_merchant_list_entity.dart';
import 'package:quandoo/features/merchant/domain/usecase/get_all_merchants_use_case.dart';
import 'package:quandoo/features/merchant/domain/usecase/get_merchant_detail_use_case.dart';
import 'package:quandoo/features/merchant/presentation/bloc/merchant_bloc.dart';
import 'package:quandoo/features/merchant/presentation/bloc/merchant_bloc_event.dart';
import 'package:quandoo/features/merchant/presentation/bloc/merchant_bloc_state.dart';

import '../../../../mock/mock_data_reader.dart';

class MockGetAllMerchantsUseCase extends Mock
    implements GetAllMerchantsUseCase {}

class MockGetMerchantDetailUseCase extends Mock
    implements GetMerchantDetailUseCase {}

void main() {
  late MockGetAllMerchantsUseCase mockGetAllMerchantsUseCase;
  late MockGetMerchantDetailUseCase mockGetMerchantDetailUseCase;
  late MerchantState initialState;
  const AllMerchantParam allMerchantParam = AllMerchantParam();
  late ApiResponse<PaginatedMerchantList> validGetAllMerchantResponse;
  late ApiResponse<PaginatedMerchantList> invalidGetAllMerchantResponse;
  late ApiResponse<Merchant> validMerchantDetailResponse;
  late ApiResponse<Merchant> invalidMerchantDetailResponse;
  late PaginatedMerchantList paginatedMerchants;
  late Merchant merchant;
  late ServerException mockServerException;
  final IdParam idParam = IdParam(id: 1);

  setUp(() {
    mockGetMerchantDetailUseCase = MockGetMerchantDetailUseCase();
    mockGetAllMerchantsUseCase = MockGetAllMerchantsUseCase();
    initialState = const MerchantState();
    paginatedMerchants = PaginatedMerchantListModel.fromJson(
        jsonDecode(getMockData("paginated_merchant_list_mock_data.json")));
    validGetAllMerchantResponse = ApiResponse(
      isSuccess: true,
      data: paginatedMerchants,
    );
    mockServerException = const ServerException();

    invalidGetAllMerchantResponse = ApiResponse(
      isSuccess: false,
      error: mockServerException,
    );

    merchant = MerchantModel.fromJson(
        jsonDecode(getMockData("merchant_mock_data.json")));
    validMerchantDetailResponse = ApiResponse(
      isSuccess: true,
      data: merchant,
    );
    invalidMerchantDetailResponse = ApiResponse(
      isSuccess: false,
      error: mockServerException,
    );
  });

  group("Merchant Bloc Test", () {
    test(
        "should return Merchant State with "
        "initial status as initial state", () {
      final bloc = MerchantBloc(
        getAllMerchantsUseCase: mockGetAllMerchantsUseCase,
        getMerchantDetailUseCase: mockGetMerchantDetailUseCase,
      );

      expect(bloc.state, initialState);
    });

    group("Get All Merchants Bloc Test", () {
      blocTest<MerchantBloc, MerchantState>(
        "should get all merchant when GetAllMerchantsEvent "
        "return valid response",
        build: () {
          when(() => mockGetAllMerchantsUseCase.call(allMerchantParam))
              .thenAnswer((_) async => validGetAllMerchantResponse);

          return MerchantBloc(
            getAllMerchantsUseCase: mockGetAllMerchantsUseCase,
            getMerchantDetailUseCase: mockGetMerchantDetailUseCase,
          );
        },
        act: (bloc) => bloc.add(
          const GetAllMerchantsEvent(param: allMerchantParam),
        ),
        expect: () => [
          initialState.copyWith(
            status: MerchantStatus.merchantsLoading,
          ),
          initialState.copyWith(
            status: MerchantStatus.merchantsLoaded,
            lastQueryParam: allMerchantParam,
            merchants: paginatedMerchants.merchants,
          )
        ],
      );

      blocTest<MerchantBloc, MerchantState>(
        "should return error when GetAllMerchantsEvent "
        "return invalid error response",
        build: () {
          when(() => mockGetAllMerchantsUseCase.call(allMerchantParam))
              .thenAnswer((_) async => invalidGetAllMerchantResponse);

          return MerchantBloc(
            getAllMerchantsUseCase: mockGetAllMerchantsUseCase,
            getMerchantDetailUseCase: mockGetMerchantDetailUseCase,
          );
        },
        act: (bloc) => bloc.add(
          const GetAllMerchantsEvent(param: allMerchantParam),
        ),
        expect: () => [
          initialState.copyWith(
            status: MerchantStatus.merchantsLoading,
          ),
          initialState.copyWith(
            status: MerchantStatus.errorLoadingMerchants,
          ),
        ],
      );
    });

    group("Get Merchants Detail Bloc Test", () {
      blocTest<MerchantBloc, MerchantState>(
        "should get merchant detail when"
        " GetMerchantDetailEvent return valid "
        "response",
        build: () {
          when(() => mockGetMerchantDetailUseCase.call(idParam))
              .thenAnswer((_) async => validMerchantDetailResponse);

          return MerchantBloc(
            getAllMerchantsUseCase: mockGetAllMerchantsUseCase,
            getMerchantDetailUseCase: mockGetMerchantDetailUseCase,
          );
        },
        act: (bloc) => bloc.add(
          GetMerchantDetailEvent(idParam),
        ),
        expect: () => [
          initialState.copyWith(
            status: MerchantStatus.detailLoading,
          ),
          initialState.copyWith(
            status: MerchantStatus.merchantDetailLoaded,
            selectedMerchant: merchant,
          )
        ],
      );

      blocTest<MerchantBloc, MerchantState>(
        "should get merchant detail when GetMerchantDetail "
        "return invalid error response",
        build: () {
          when(() => mockGetMerchantDetailUseCase.call(idParam))
              .thenAnswer((_) async => invalidMerchantDetailResponse);

          return MerchantBloc(
            getAllMerchantsUseCase: mockGetAllMerchantsUseCase,
            getMerchantDetailUseCase: mockGetMerchantDetailUseCase,
          );
        },
        act: (bloc) => bloc.add(
          GetMerchantDetailEvent(idParam),
        ),
        expect: () => [
          initialState.copyWith(
            status: MerchantStatus.detailLoading,
          ),
          initialState.copyWith(
            status: MerchantStatus.errorLoadingDetail,
          ),
        ],
      );
    });
  });
}
