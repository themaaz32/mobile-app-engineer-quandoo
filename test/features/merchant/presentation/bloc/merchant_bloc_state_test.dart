import 'package:flutter_test/flutter_test.dart';
import 'package:quandoo/core/models/all_merchant_usecase_param.dart';
import 'package:quandoo/features/merchant/presentation/bloc/merchant_bloc_state.dart';

void main() {
  const merchantState =
      MerchantState(lastQueryParam: AllMerchantParam(offset: 40));

  group('MerchantState', () {
    test('supports value comparisons', () {
      expect(const MerchantState(), const MerchantState());
    });

    test('returns same object when no properties are passed', () {
      expect(const MerchantState().copyWith(), const MerchantState());
    });

    test('returns object with updated status when status is passed', () {
      expect(
        const MerchantState()
            .copyWith(lastQueryParam: const AllMerchantParam(offset: 40)),
        merchantState,
      );
    });
  });
}
