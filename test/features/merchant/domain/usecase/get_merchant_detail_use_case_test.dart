import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:quandoo/core/models/api_response_model.dart';
import 'package:quandoo/features/merchant/data/model/merchant_model.dart';
import 'package:quandoo/features/merchant/domain/repository_contract/merchant_repository_contract.dart';
import 'package:quandoo/features/merchant/domain/usecase/get_merchant_detail_use_case.dart';


class MockMerchantRepository extends Mock implements MerchantRepository{}

void main() {
  late GetMerchantDetailUseCase useCase;
  late MockMerchantRepository mockMerchantRepository;

  setUp(() {
    mockMerchantRepository = MockMerchantRepository();
    useCase = GetMerchantDetailUseCase(mockMerchantRepository);
  });

  const int merchantId = 123;
  const fakeResponse = ApiResponse<MerchantModel>(isSuccess: true);

  test("should get a merchant detail", () async {
    //arrange
    when(()=>mockMerchantRepository.getMerchantDetail(merchantId))
        .thenAnswer((_) async => fakeResponse);

    //act
    final result = await useCase(IdParam(id: merchantId));

    //assert
    expect(result, fakeResponse);
    verify(()=>mockMerchantRepository.getMerchantDetail(merchantId));
    verifyNoMoreInteractions(mockMerchantRepository);
  });
}
