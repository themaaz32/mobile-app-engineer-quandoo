import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:quandoo/core/models/all_merchant_usecase_param.dart';
import 'package:quandoo/core/models/api_response_model.dart';
import 'package:quandoo/features/merchant/data/model/paginated_merchant_list_model.dart';
import 'package:quandoo/features/merchant/domain/repository_contract/merchant_repository_contract.dart';
import 'package:quandoo/features/merchant/domain/usecase/get_all_merchants_use_case.dart';


class MockMerchantRepository extends Mock implements MerchantRepository{}

void main() {
  late GetAllMerchantsUseCase useCase;
  late MockMerchantRepository mockMerchantRepository;

  setUp(() {
    mockMerchantRepository = MockMerchantRepository();
    useCase = GetAllMerchantsUseCase(mockMerchantRepository);
  });

  const fakeResponse = ApiResponse<PaginatedMerchantListModel>(isSuccess: true);

  test("should get list of all the first 20 merchants", () async {
    //arrange
    when(()=>mockMerchantRepository.getAllMerchants())
        .thenAnswer((_) async => fakeResponse);

    //act
    final result = await useCase(const AllMerchantParam());

    //assert
    expect(result, fakeResponse);
    verify(()=>mockMerchantRepository.getAllMerchants());
    verifyNoMoreInteractions(mockMerchantRepository);
  });
}
