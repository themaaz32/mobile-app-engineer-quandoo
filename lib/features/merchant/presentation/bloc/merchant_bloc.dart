import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:quandoo/core/models/api_response_model.dart';
import 'package:quandoo/features/merchant/domain/entity/merchant_entity.dart';
import 'package:quandoo/features/merchant/domain/entity/paginated_merchant_list_entity.dart';
import 'package:quandoo/features/merchant/domain/usecase/get_all_merchants_use_case.dart';
import 'package:quandoo/features/merchant/domain/usecase/get_merchant_detail_use_case.dart';
import 'package:quandoo/features/merchant/presentation/bloc/merchant_bloc_event.dart';
import 'package:quandoo/features/merchant/presentation/bloc/merchant_bloc_state.dart';

class MerchantBloc extends Bloc<MerchantEvent, MerchantState> {
  final GetAllMerchantsUseCase getAllMerchantsUseCase;
  final GetMerchantDetailUseCase getMerchantDetailUseCase;

  MerchantBloc({
    required this.getAllMerchantsUseCase,
    required this.getMerchantDetailUseCase,
  }) : super(const MerchantState()) {
    on<GetAllMerchantsEvent>(_onGetAllMerchantsEvent);
    on<GetMerchantDetailEvent>(_onGetMerchantDetailEvent);
  }

  Future<void> _onGetAllMerchantsEvent(
      GetAllMerchantsEvent event, Emitter<MerchantState> emit) async {
    emit(state.copyWith(status: MerchantStatus.merchantsLoading));

    final ApiResponse<PaginatedMerchantList> response =
    await getAllMerchantsUseCase(event.param);

    if (response.isSuccess) {
      print(event.param.offset);
      emit(state.copyWith(
        lastQueryParam: event.param,
        merchants: event.param.isFirstPage
            ? response.data!.merchants
            : [...state.merchants, ...response.data!.merchants],
        status: MerchantStatus.merchantsLoaded,
      ));
    } else {
      emit(state.copyWith(status: MerchantStatus.errorLoadingMerchants));
    }
  }



  Future<void> _onGetMerchantDetailEvent(
      GetMerchantDetailEvent event, Emitter<MerchantState> emit) async {
    emit(state.copyWith(status: MerchantStatus.detailLoading));

    final ApiResponse<Merchant> response =
        await getMerchantDetailUseCase(event.idParam);

    if (response.isSuccess) {
      emit(
        state.copyWith(
          selectedMerchant: response.data,
          status: MerchantStatus.merchantDetailLoaded,
        ),
      );
    }else{
      emit(state.copyWith(status: MerchantStatus.errorLoadingDetail));
    }
  }

  @override
  void onEvent(MerchantEvent event) {
    print("${event.runtimeType} Event");
    super.onEvent(event);
  }

  @override
  void onChange(Change<MerchantState> change) {
    super.onChange(change);
    print('onChange --, current : ${change.currentState.status}, next : ${change.nextState.status}');
  }

  @override
  void onTransition(Transition<MerchantEvent, MerchantState> transition) {
    super.onTransition(transition);
    print('onTransition --, ${transition.event.runtimeType}: ${transition.currentState.runtimeType} (${transition.currentState.status}) -> ${transition.nextState.runtimeType} (${transition.nextState.status})');
  }
}
