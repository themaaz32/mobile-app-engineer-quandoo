import 'package:equatable/equatable.dart';
import 'package:quandoo/core/models/all_merchant_usecase_param.dart';
import 'package:quandoo/features/merchant/domain/usecase/get_merchant_detail_use_case.dart';

abstract class MerchantEvent extends Equatable {
  const MerchantEvent();

  @override
  List<Object?> get props => [];
}

class GetAllMerchantsEvent extends MerchantEvent {
  const GetAllMerchantsEvent({
    required this.param,
  });

  final AllMerchantParam param;

  @override
  List<Object?> get props => [param];
}


class GetMerchantDetailEvent extends MerchantEvent {
  const GetMerchantDetailEvent(this.idParam);

  final IdParam idParam;

  @override
  // TODO: implement props
  List<Object?> get props => [idParam];
}

class ResetMerchantListEvent extends MerchantEvent{
  const ResetMerchantListEvent();
}
