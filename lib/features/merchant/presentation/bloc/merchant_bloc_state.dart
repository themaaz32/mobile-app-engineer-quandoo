import 'package:equatable/equatable.dart';
import 'package:quandoo/core/models/all_merchant_usecase_param.dart';
import 'package:quandoo/features/merchant/domain/entity/merchant_entity.dart';

class MerchantState extends Equatable {
  const MerchantState({
    this.merchants = const [],
    this.selectedMerchant,
    this.status = MerchantStatus.initial,
    this.lastQueryParam = const AllMerchantParam(offset: 0),
  });

  final MerchantStatus status;

  final List<Merchant> merchants;
  final Merchant? selectedMerchant;
  final AllMerchantParam lastQueryParam;

  @override
  List<Object?> get props =>
      [status, merchants, lastQueryParam, selectedMerchant];

  MerchantState copyWith({
    List<Merchant>? merchants,
    AllMerchantParam? lastQueryParam,
    Merchant? selectedMerchant,
    MerchantStatus? status,
  }) {
    return MerchantState(
      merchants: merchants ?? this.merchants,
      selectedMerchant: selectedMerchant ?? this.selectedMerchant,
      status: status ?? this.status,
      lastQueryParam: lastQueryParam ?? this.lastQueryParam,
    );
  }
}

enum MerchantStatus {
  initial,
  merchantsLoading,
  detailLoading,
  merchantsLoaded,
  merchantDetailLoaded,
  errorLoadingMerchants,
  errorLoadingDetail,
}
