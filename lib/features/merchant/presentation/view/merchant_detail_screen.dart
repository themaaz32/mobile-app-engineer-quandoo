import 'package:flutter/material.dart';
import 'package:quandoo/core/style/color.dart';
import 'package:quandoo/features/merchant/presentation/bloc/merchant_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:quandoo/features/merchant/presentation/widgets/floating_back_button.dart';
import 'package:quandoo/features/merchant/presentation/widgets/image_slider.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:quandoo/features/merchant/presentation/widgets/merchant_tags.dart';

class MerchantDetailScreen extends StatelessWidget {
  const MerchantDetailScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<MerchantBloc>();
    final merchant = bloc.state.selectedMerchant!;
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      MerchantImageSlider(
                        images: merchant.images,
                        height: MediaQuery.of(context).size.height*0.6,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(24.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              merchant.name,
                              style: Theme.of(context)
                                  .textTheme
                                  .headline3!
                                  .copyWith(color: Colors.black),
                            ),
                            const SizedBox(
                              height: 8,
                            ),
                            Row(
                              children: [
                                const Icon(
                                  Icons.phone,
                                  size: 14,
                                ),
                                const SizedBox(
                                  width: 4,
                                ),
                                Text(
                                  merchant.phoneNumber,
                                  style: Theme.of(context).textTheme.subtitle1,
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 4,
                            ),
                            Text(
                              merchant.location.address.toString(),
                              style: Theme.of(context).textTheme.caption,
                            ),
                            const SizedBox(
                              height: 8,
                            ),
                            Row(
                              children: [
                                const Icon(
                                  Icons.star,
                                  color: primaryColor,
                                  size: 18,
                                ),
                                const SizedBox(
                                  width: 8,
                                ),
                                Text(
                                  "${merchant.reviewScore} pts",
                                  style: Theme.of(context).textTheme.subtitle1,
                                ),
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  flex: 2,
                ),
                Expanded(
                  child: Container(
                    color: Colors.white,
                    padding: const EdgeInsets.all(24),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Details",
                          style: Theme.of(context).textTheme.headline4,
                        ),
                        const SizedBox(height: 16,),
                        MerchantTags(
                          tags: merchant.tagGroup,
                          label: "Price Type",
                          tagValue: "PRICE",
                        ),

                        MerchantTags(
                          tags: merchant.tagGroup,
                          label: "Cuisine Type",
                          tagValue: "CUISINE",
                        ),

                        MerchantTags(
                          tags: merchant.tagGroup,
                          label: "Meal Type",
                          tagValue: "MEAL_TYPE",
                        ),
                        MerchantTags(
                          tags: merchant.tagGroup,
                          label: "Meal Type",
                          tagValue: "GOOD_FOR",
                        ),
                        MerchantTags(
                          tags: merchant.tagGroup,
                          label: "Menu Detail",
                          tagValue: "MENU_DETAILS",
                        ),
                        MerchantTags(
                          tags: merchant.tagGroup,
                          label: "Internet Type",
                          tagValue: "INTERNET",
                        ),
                        MerchantTags(
                          tags: merchant.tagGroup,
                          label: "Payment Type",
                          tagValue: "PAYMENT",
                        ), MerchantTags(
                          tags: merchant.tagGroup,
                          label: "Payment Type",
                          tagValue: "FOOD_RELATED",
                        ),
                      ],
                    ),
                  ),
                  flex: 1,
                )
              ],
            ),
            const FloatingBackButton(),
          ],
        ),
      ),
    );
  }
}
