import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:quandoo/core/constants/routes.dart';
import 'package:quandoo/features/merchant/presentation/bloc/merchant_bloc.dart';
import 'package:quandoo/features/merchant/presentation/bloc/merchant_bloc_state.dart';
import 'package:quandoo/features/merchant/presentation/widgets/merchants_grid.dart';

class MerchantListingScreen extends StatelessWidget {
  const MerchantListingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<MerchantBloc>();
    return Scaffold(
      body: BlocListener<MerchantBloc, MerchantState>(
        bloc: bloc,
        listenWhen: (prev, current) {
          return prev.status != current.status;
        },
        listener: (context, state) {
          if (state.status == MerchantStatus.merchantDetailLoaded) {
            if(EasyLoading.isShow){
              EasyLoading.dismiss();
            }
            Navigator.pushNamed(context, merchantDetailScreen);
          } else if (state.status == MerchantStatus.errorLoadingMerchants) {
            EasyLoading.showError('Error Loading Merchant Detail');
          }
          else if (state.status == MerchantStatus.detailLoading) {
            EasyLoading.show();
          }
        },
        child: MerchantGrid(),
      ),
    );
  }
}
