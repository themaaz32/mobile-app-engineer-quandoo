import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:quandoo/features/merchant/domain/entity/image_entity.dart'
as merchant;

class MerchantImageSlider extends StatelessWidget {
  const MerchantImageSlider(
      {Key? key, required this.images, required this.height}) : super(key: key);

  final List<merchant.Image> images;
  final double height;

  @override
  Widget build(BuildContext context) {

    return CarouselSlider(
      options: CarouselOptions(height: height, autoPlay: true, viewportFraction: 1),
      items: images.map((i) {
        return Builder(
          builder: (BuildContext context) {
            return SizedBox(
              width: double.infinity,
              child: Image.network(i.url, fit: BoxFit.cover,),
            );
          },
        );
      }).toList(),
    );
  }
}
