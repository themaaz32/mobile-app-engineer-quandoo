import 'package:flutter/material.dart';
import 'package:quandoo/core/constants/constants.dart';
import 'package:quandoo/core/style/color.dart';
import 'package:quandoo/features/merchant/domain/entity/merchant_entity.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MerchantTile extends StatelessWidget {
  const MerchantTile({Key? key, required this.merchant}) : super(key: key);

  final Merchant merchant;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) => Container(
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(24),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey[100]!,
                  spreadRadius: 5,
                  blurRadius: 10,
                )
              ]),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                child: SizedBox(
                  height: constraints.maxHeight * 0.55,
                  width: constraints.maxWidth,
                  child: Image.network(merchant.images.isNotEmpty?merchant.images.first.url:noImageUrl, fit: BoxFit.cover,),
                ),
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(24),
                  topRight: Radius.circular(24),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      merchant.name,
                      style:  TextStyle(
                        fontSize: 32.sp,
                        fontWeight: FontWeight.w500,
                      ),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                    Text(
                      "Rating : ${merchant.reviewScore}",
                      style:  TextStyle(
                        fontSize: 20.sp,
                        color: greyTextColor,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }
}
