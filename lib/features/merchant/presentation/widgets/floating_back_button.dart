import 'package:flutter/material.dart';
import 'package:quandoo/core/style/color.dart';

class FloatingBackButton extends StatelessWidget {
  const FloatingBackButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        Navigator.pop(context);
      },
      icon: const Icon(Icons.arrow_back_ios),
      color: whiteColor,
    );
  }
}
