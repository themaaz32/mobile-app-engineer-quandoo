import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:quandoo/core/constants/constants.dart';
import 'package:quandoo/core/models/all_merchant_usecase_param.dart';

import 'package:quandoo/features/merchant/domain/usecase/get_merchant_detail_use_case.dart';
import 'package:quandoo/features/merchant/presentation/bloc/merchant_bloc.dart';
import 'package:quandoo/features/merchant/presentation/bloc/merchant_bloc_event.dart';
import 'package:quandoo/features/merchant/presentation/bloc/merchant_bloc_state.dart';
import 'package:quandoo/features/merchant/presentation/widgets/merchant_tile.dart';

class MerchantGrid extends StatefulWidget {
  MerchantGrid({Key? key}) : super(key: key);

  @override
  State<MerchantGrid> createState() => _MerchantGridState();
}

class _MerchantGridState extends State<MerchantGrid> {
  final controller = RefreshController();

  @override
  void initState() {
    Future.delayed(Duration.zero, (){
      controller.requestRefresh();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<MerchantBloc>();

    return BlocConsumer<MerchantBloc, MerchantState>(
      bloc: bloc,
      listener: (context, state) {

        if(state.status == MerchantStatus.merchantsLoaded){
          if(state.lastQueryParam.offset == 0){
            controller.refreshCompleted();
          }else{
            controller.loadComplete();
          }
        }else if(state.status == MerchantStatus.errorLoadingMerchants){
          if(state.lastQueryParam.offset == 0){
            controller.refreshFailed();
          }else{
            controller.loadFailed();
          }
        }
      },
      builder: (context, state) {
        if (state.status != MerchantStatus.errorLoadingMerchants) {
          return SmartRefresher(
            controller: controller,
            enablePullUp: true,
            onLoading: (){
              bloc.add(GetAllMerchantsEvent(param: AllMerchantParam(offset: (bloc.state.lastQueryParam.offset!+merchantRecordsLimitConstant))));
            },
            onRefresh: (){
              bloc.add(const GetAllMerchantsEvent(param: AllMerchantParam(offset: 0)));
            },
            child: GridView.builder(
              padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 8),
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                crossAxisSpacing: 16,
                mainAxisSpacing: 16,
              ),
              itemBuilder: (context, index){
                final item = bloc.state.merchants.elementAt(index);
                return GestureDetector(
                  onTap: () {
                    bloc.add(GetMerchantDetailEvent(IdParam(id: item.id)));
                  },
                  child: MerchantTile(
                    merchant: item,
                  ),
                );
              },
              itemCount: bloc.state.merchants.length,
            ),
          );
        } else {
          return const Center(child: Text('failed to fetch merchants'));
        }
      },
    );
  }
}
