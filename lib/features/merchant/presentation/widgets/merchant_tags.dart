import 'package:flutter/material.dart';
import 'package:quandoo/features/merchant/domain/entity/tag_group_entity.dart';

class MerchantTags extends StatelessWidget {
  const MerchantTags({
    Key? key,
    required this.label,
    required this.tags,
    required this.tagValue,
  }) : super(key: key);

  final String label;
  final String tagValue;
  final List<TagGroup> tags;

  @override
  Widget build(BuildContext context) {
    return tags.where((element) => element.type.compareTo(tagValue) == 0).isEmpty ? const SizedBox() : Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: Theme.of(context).textTheme.headline6,
        ),
        Wrap(
            runSpacing: 16,
            spacing: 16,
            children: tags
                .where((element) =>
            element.type.compareTo(tagValue) == 0, )
                .first
                .tags
                .map((e) => Chip(label: Text(e.name)))
                .toList()),
        const SizedBox(height: 16,),
      ],
    );
  }
}
