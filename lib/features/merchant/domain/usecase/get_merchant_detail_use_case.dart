import 'package:quandoo/core/models/api_response_model.dart';
import 'package:quandoo/core/usecase/usecase_contract.dart';
import 'package:quandoo/features/merchant/domain/entity/merchant_entity.dart';
import 'package:quandoo/features/merchant/domain/repository_contract/merchant_repository_contract.dart';

class IdParam{
  final int id;

  IdParam({required this.id});
}

class GetMerchantDetailUseCase extends UseCase<ApiResponse, IdParam>{

  final MerchantRepository _merchantRepository;

  GetMerchantDetailUseCase(this._merchantRepository);

  @override
  Future<ApiResponse<Merchant>> call(IdParam param) async{
    return await _merchantRepository.getMerchantDetail(param.id);
  }

}