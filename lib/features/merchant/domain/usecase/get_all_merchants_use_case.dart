import 'package:quandoo/core/models/all_merchant_usecase_param.dart';
import 'package:quandoo/core/models/api_response_model.dart';

import 'package:quandoo/core/usecase/usecase_contract.dart';
import 'package:quandoo/features/merchant/domain/entity/paginated_merchant_list_entity.dart';
import 'package:quandoo/features/merchant/domain/repository_contract/merchant_repository_contract.dart';

class GetAllMerchantsUseCase implements UseCase<ApiResponse<PaginatedMerchantList>, AllMerchantParam> {
  final MerchantRepository _repository;

  GetAllMerchantsUseCase(this._repository);

  @override
  Future<ApiResponse<PaginatedMerchantList>> call(AllMerchantParam param) async {
    return await _repository.getAllMerchants(query: param.query, offset: param.offset ?? 0, limit: param.limit);
  }
}
