import 'package:quandoo/core/models/api_response_model.dart';
import 'package:quandoo/features/merchant/data/model/merchant_model.dart';
import 'package:quandoo/features/merchant/data/model/paginated_merchant_list_model.dart';

abstract class MerchantRepository{
  Future<ApiResponse<PaginatedMerchantListModel>> getAllMerchants({String query = "", int offset=0, int limit=20});
  Future<ApiResponse<MerchantModel>> getMerchantDetail(int id);
}