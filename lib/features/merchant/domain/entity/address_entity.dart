import 'package:equatable/equatable.dart';

class Address extends Equatable {
  final String? street;
  final String? number;
  final String? zipCode;
  final String? city;
  final String? country;
  final String? district;

  const Address({
    this.street,
    this.number,
    this.zipCode,
    this.city,
    this.country,
    this.district,
  });

  String toString() => "$street $number, $city, $country";

  @override
  List<Object?> get props => [street, number, zipCode, city, country,district];
}
