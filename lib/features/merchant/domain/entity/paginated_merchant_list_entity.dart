import 'package:equatable/equatable.dart';
import 'package:quandoo/features/merchant/domain/entity/merchant_entity.dart';

class PaginatedMerchantList extends Equatable{
  final List<Merchant> merchants;
  final int size;
  final int offset;
  final int limit;

  const PaginatedMerchantList({
    required this.merchants,
    required this.size,
    required this.offset,
    required this.limit,
  });

  @override
  List<Object?> get props => [merchants, size, offset, limit];
}
