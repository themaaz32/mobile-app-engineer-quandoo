import 'package:equatable/equatable.dart';

class Timing extends Equatable{
  final String start;
  final String end;

  const Timing({required this.start, required this.end});

  @override
  // TODO: implement props
  List<Object?> get props => [start, end];

}