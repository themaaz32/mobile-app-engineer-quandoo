import 'package:equatable/equatable.dart';
import 'package:quandoo/features/merchant/domain/entity/address_entity.dart';
import 'package:quandoo/features/merchant/domain/entity/coordinate_entity.dart';

class Location extends Equatable{
  final Address address;
  final Coordinates coordinates;


  const Location({required this.address, required this.coordinates});

  @override
  List<Object?> get props => [address, coordinates];
}