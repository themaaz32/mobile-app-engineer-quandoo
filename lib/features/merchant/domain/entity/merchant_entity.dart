import 'package:equatable/equatable.dart';
import 'package:quandoo/features/merchant/domain/entity/location_entity.dart';
import 'package:quandoo/features/merchant/domain/entity/opening_time_entity.dart';
import 'package:quandoo/features/merchant/domain/entity/tag_group_entity.dart';

import 'image_entity.dart';

class Merchant extends Equatable {
  final int id;
  final String name;
  final String phoneNumber;
  final String currency;
  final String locale;
  final String timeZone;
  final Location location;
  final List<TagGroup> tagGroup;
  final List<Image> images;
  final String reviewScore;
  final OpeningTime openingTime;
  final bool bookable;
  final bool cvcEnabled;

  const Merchant({
    required this.id,
    required this.name,
    required this.phoneNumber,
    required this.currency,
    required this.locale,
    required this.timeZone,
    required this.location,
    required this.tagGroup,
    required this.reviewScore,
    required this.images,
    required this.bookable,
    required this.openingTime,
    required this.cvcEnabled,
  });

  @override
  List<Object?> get props => [
        id,
        name,
        phoneNumber,
        currency,
        location,
        timeZone,
        location,
        tagGroup,
        images,
        bookable,
        cvcEnabled,
      ];
}
