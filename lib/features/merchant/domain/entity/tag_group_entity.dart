import 'package:equatable/equatable.dart';
import 'package:quandoo/features/merchant/domain/entity/tag_entity.dart';

class TagGroup extends Equatable{

  final String type;
  final List<Tag> tags;

  const TagGroup({required this.type, required this.tags});

  @override
  // TODO: implement props
  List<Object?> get props => [type, tags];

}