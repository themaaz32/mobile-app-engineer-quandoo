import 'package:equatable/equatable.dart';
import 'package:quandoo/features/merchant/domain/entity/timing_entity.dart';

class OpeningTime extends Equatable{

  final Map<String, Map<String, List<Timing>>> standardOpeningTimes;

  const OpeningTime(this.standardOpeningTimes);

  @override
  // TODO: implement props
  List<Object?> get props => [standardOpeningTimes];

}