import 'package:quandoo/features/merchant/data/model/merchant_model.dart';
import 'package:quandoo/features/merchant/domain/entity/paginated_merchant_list_entity.dart';

class PaginatedMerchantListModel extends PaginatedMerchantList {
  final List<MerchantModel> _merchants;

  const PaginatedMerchantListModel(
     this._merchants,
    int _size,
    int _offset,
    int _limit,
  ) : super(
          merchants: _merchants,
          limit: _limit,
          size: _size,
          offset: _offset,
        );

  factory PaginatedMerchantListModel.fromJson(Map<String, dynamic> json) => PaginatedMerchantListModel(
    List<MerchantModel>.from(json["merchants"].map((x) => MerchantModel.fromJson(x))),
    json["size"],
    json["offset"],
    json["limit"],
  );

  Map<String, dynamic> toJson() => {
    "merchants": List<dynamic>.from(_merchants.map((x) => x.toJson())),
    "size": size,
    "offset": offset,
    "limit": limit,
  };

}
