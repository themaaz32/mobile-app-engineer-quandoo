import 'package:quandoo/features/merchant/data/model/timing_model.dart';
import 'package:quandoo/features/merchant/domain/entity/opening_time_entity.dart';

class OpeningTimeModel extends OpeningTime {
  final Map<String, Map<String, List<TimingModel>>> _standardOpeningTimes;

  const OpeningTimeModel(this._standardOpeningTimes)
      : super(_standardOpeningTimes);

  factory OpeningTimeModel.fromJson(Map<String, dynamic> json) =>
      OpeningTimeModel(
        {
          "standardOpeningTimes": Map.fromEntries(
            (json["standardOpeningTimes"] as Map<String, dynamic>).keys.map(
                  (key) => MapEntry(
                    key,
                    List.from(
                      (((json["standardOpeningTimes"]!
                          as Map<String, dynamic>)[key]) as List<dynamic>),
                    ).map((e) => TimingModel.fromJson(e)).toList(),
                  ),
                ),
          ),
        },
      );

  Map<String, dynamic> toJson() => {
        "standardOpeningTimes": Map.fromEntries(
          (_standardOpeningTimes["standardOpeningTimes"]
                  as Map<String, List<TimingModel>>)
              .entries
              .map(
                (e) => MapEntry(
                  e.key,
                  e.value.map(
                    (e) => e.toJson(),
                  ),
                ),
              ),
        ),
      };
}
