import 'package:quandoo/features/merchant/domain/entity/image_entity.dart';

class ImageModel extends Image{
  const ImageModel(String url) : super(url);

  factory ImageModel.fromJson(Map<String, dynamic> json) => ImageModel(
    json["url"],
  );

  Map<String, dynamic> toJson() => {
    "url": url,
  };
}