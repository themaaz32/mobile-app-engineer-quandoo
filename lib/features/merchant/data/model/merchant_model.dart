import 'package:quandoo/features/merchant/data/model/location_model.dart';
import 'package:quandoo/features/merchant/data/model/tag_group_model.dart';
import 'package:quandoo/features/merchant/domain/entity/merchant_entity.dart';

import 'image_model.dart';
import 'opening_time_model.dart';

class MerchantModel extends Merchant {
  final LocationModel _location;
  final List<TagGroupModel> _tagGroup;
  final List<ImageModel> _images;
  final OpeningTimeModel _openingTime;

  const MerchantModel(
    int _id,
    String _name,
    String _phoneNumber,
    String _currency,
    String _locale,
    String _timeZone,
    this._location,
    this._tagGroup,
    String _reviewScore,
    this._images,
    bool _bookable,
    this._openingTime,
    bool _cvcEnabled,
  ) : super(
          id: _id,
          name: _name,
          phoneNumber: _phoneNumber,
          currency: _currency,
          locale: _locale,
          timeZone: _timeZone,
          location: _location,
          tagGroup: _tagGroup,
          reviewScore: _reviewScore,
          images: _images,
          bookable: _bookable,
          openingTime: _openingTime,
          cvcEnabled: _cvcEnabled,
        );

  factory MerchantModel.fromJson(Map<String, dynamic> json) => MerchantModel(
        json["id"],
        json["name"],
        json["phoneNumber"],
        json["currency"],
        json["locale"],
        json["timezone"],
        LocationModel.fromJson(json["location"]),
        List<TagGroupModel>.from(
            json["tagGroups"].map((x) => TagGroupModel.fromJson(x))),
        json["reviewScore"],
        List<ImageModel>.from(
            json["images"].map((x) => ImageModel.fromJson(x))),
        json["bookable"],
        OpeningTimeModel.fromJson(json["openingTimes"]),
        json["ccvEnabled"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "phoneNumber": phoneNumber,
        "currency": currency,
        "locale": locale,
        "timezone": timeZone,
        "location": _location.toJson(),
        "reviewScore": reviewScore,
        "tagGroups": List<dynamic>.from(_tagGroup.map((x) => x.toJson())),
        "images": List<dynamic>.from(_images.map((x) => x.toJson())),
        "bookable": bookable,
        "openingTimes": _openingTime.toJson(),
        "ccvEnabled": cvcEnabled,
      };
}
