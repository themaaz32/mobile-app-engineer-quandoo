import 'package:quandoo/features/merchant/domain/entity/coordinate_entity.dart';

class CoordinateModel extends Coordinates {
  const CoordinateModel({required double longitude, required double latitude})
      : super(
          latitude: latitude,
          longitude: longitude,
        );

  factory CoordinateModel.fromJson(Map<String, dynamic> json) =>
      CoordinateModel(
        latitude: json["latitude"].toDouble(),
        longitude: json["longitude"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "latitude": latitude,
        "longitude": longitude,
      };
}
