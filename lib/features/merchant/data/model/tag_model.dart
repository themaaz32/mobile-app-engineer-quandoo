import 'package:quandoo/features/merchant/domain/entity/tag_entity.dart';

class TagModel extends Tag {
  const TagModel({required String id, required String name})
      : super(
          id: id,
          name: name,
        );

  factory TagModel.fromJson(Map<String, dynamic> json) => TagModel(
    id: json["id"],
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
  };
}
