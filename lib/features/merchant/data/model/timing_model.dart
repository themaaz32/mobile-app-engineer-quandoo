import 'package:quandoo/features/merchant/domain/entity/timing_entity.dart';

class TimingModel extends Timing {
  const TimingModel({
    required String start,
    required String end,
  }) : super(
          start: start,
          end: end,
        );

  factory TimingModel.fromJson(Map<String, dynamic> json) => TimingModel(
    start: json["start"],
    end: json["end"],
  );

  Map<String, dynamic> toJson() => {
    "start": start,
    "end": end,
  };
}
