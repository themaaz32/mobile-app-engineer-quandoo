import 'package:quandoo/features/merchant/data/model/coordinate_model.dart';
import 'package:quandoo/features/merchant/domain/entity/location_entity.dart';

import 'address_model.dart';

class LocationModel extends Location {

  final AddressModel _addressModel;
  final CoordinateModel _coordinatesModel;

  const LocationModel(
    this._addressModel,
    this._coordinatesModel,
  ) : super(
    address: _addressModel,
    coordinates: _coordinatesModel,
  );

  factory LocationModel.fromJson(Map<String, dynamic> json) => LocationModel(
    AddressModel.fromJson(json["address"]),
    CoordinateModel.fromJson(json["coordinates"]),
  );

  Map<String, dynamic> toJson() => {
    "coordinates": _coordinatesModel.toJson(),
    "address": _addressModel.toJson(),
  };
}
