import 'package:quandoo/features/merchant/domain/entity/address_entity.dart';

class AddressModel extends Address {
  const AddressModel({
    String? street,
    String? number,
    String? zipCode,
    String? city,
    String? country,
    String? district,
  }) : super(
          street: street,
          number: number,
          zipCode: zipCode,
          city: city,
          country: country,
          district: district,
        );

  factory AddressModel.fromJson(Map<String, dynamic> json) => AddressModel(
    street: json["street"],
    number: json["number"],
    zipCode: json["zipcode"],
    city: json["city"],
    country: json["country"],
    district: json["district"],
  );

  Map<String, dynamic> toJson() => {
    "street": street,
    "number": number,
    "zipcode": zipCode,
    "city": city,
    "country": country,
    "district": district,
  };
}
