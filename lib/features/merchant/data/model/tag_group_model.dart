import 'package:quandoo/features/merchant/data/model/tag_model.dart';
import 'package:quandoo/features/merchant/domain/entity/tag_group_entity.dart';

class TagGroupModel extends TagGroup {

  final List<TagModel> _tags;

  const TagGroupModel(String _type,this._tags)
      : super(
          tags: _tags,
          type: _type,
        );

  factory TagGroupModel.fromJson(Map<String, dynamic> json) => TagGroupModel(
    json["type"],
    List<TagModel>.from(json["tags"].map((x) => TagModel.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "type": type,
    "tags": List<dynamic>.from(_tags.map((x) => x.toJson())),
  };
}
