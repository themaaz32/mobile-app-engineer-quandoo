import 'package:quandoo/core/error/exceptions.dart';
import 'package:quandoo/core/models/api_response_model.dart';
import 'package:quandoo/features/merchant/data/data_sources/merchant_remote_data_source.dart';
import 'package:quandoo/features/merchant/data/model/merchant_model.dart';
import 'package:quandoo/features/merchant/data/model/paginated_merchant_list_model.dart';
import 'package:quandoo/features/merchant/domain/repository_contract/merchant_repository_contract.dart';

class MerchantRepositoryImplementation extends MerchantRepository {
  MerchantRepositoryImplementation(this._merchantRemoteDataSource);

  final MerchantRemoteDataSource _merchantRemoteDataSource;

  @override
  Future<ApiResponse<PaginatedMerchantListModel>> getAllMerchants(
      {String query = "", int offset = 0, int limit = 20}) async {
    try {
      final result = await _merchantRemoteDataSource.getAllMerchants(
        query: query,
        offset: offset,
        limit: limit,
      );
      final response = ApiResponse(
        isSuccess: true,
        data: result,
      );

      return response;
    } on ServerException catch (e) {
      return ApiResponse(isSuccess: false, error: e);
    }
  }

  @override
  Future<ApiResponse<MerchantModel>> getMerchantDetail(int id) async {
    try {
      final result = await _merchantRemoteDataSource.getMerchantDetail(id);
      final response = ApiResponse(
        isSuccess: true,
        data: result,
      );

      return response;
    } on ServerException catch (e) {
      return ApiResponse(isSuccess: false, error: e);
    }
  }
}
