import 'package:quandoo/features/merchant/data/model/merchant_model.dart';
import 'package:quandoo/features/merchant/data/model/paginated_merchant_list_model.dart';

abstract class MerchantRemoteDataSource{
  Future<PaginatedMerchantListModel> getAllMerchants({String query = "", int offset=0, int limit=20});
  Future<MerchantModel> getMerchantDetail(int id);
}