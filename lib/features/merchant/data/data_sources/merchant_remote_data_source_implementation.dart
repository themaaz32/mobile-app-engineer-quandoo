import 'package:quandoo/core/constants/constants.dart';
import 'package:quandoo/core/data/remote_client.dart';
import 'package:quandoo/core/error/exceptions.dart';
import 'package:quandoo/core/models/api_request_model.dart';
import 'package:quandoo/features/merchant/data/data_sources/merchant_remote_data_source.dart';
import 'package:quandoo/features/merchant/data/model/merchant_model.dart';
import 'package:quandoo/features/merchant/data/model/paginated_merchant_list_model.dart';

class MerchantRemoteDataSourceImplementation extends MerchantRemoteDataSource {
  MerchantRemoteDataSourceImplementation({required this.remoteClient});

  final RemoteClient remoteClient;

  @override
  Future<PaginatedMerchantListModel> getAllMerchants({
    String query = "",
    int offset = 0,
    int limit = 20,
  }) async {
    final ApiRequest request = ApiRequest(
      path: "/v1/merchants",
      query: {
        queryConstant: query,
        offsetConstant: offset,
        limitConstant: limit,
      },
    );

    try {
      final result = await remoteClient.get(request: request);

      final paginatedMerchantListModel =
          PaginatedMerchantListModel.fromJson(result.data);

      return paginatedMerchantListModel;
    } on ServerException {
      rethrow;
    }
  }

  @override
  Future<MerchantModel> getMerchantDetail(int id) async {
    final ApiRequest request = ApiRequest(
      path: "/v1/merchants/$id",
    );

    try {
      final result = await remoteClient.get(request: request);

      final merchantModel = MerchantModel.fromJson(result.data);

      return merchantModel;
    } on ServerException {
      rethrow;
    }
  }
}
