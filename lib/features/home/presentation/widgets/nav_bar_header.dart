import 'package:flutter/material.dart';
import 'package:quandoo/core/style/color.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:quandoo/core/widgets/quandoo_logo.dart';

class NavBarHeader extends StatelessWidget {
  const NavBarHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200.h,
      color: primaryColor,
      child: Center(
        child: QuandooLogo(
          width: 100.h,
          height: 100.h,
        ),
      ),
    );
  }
}
