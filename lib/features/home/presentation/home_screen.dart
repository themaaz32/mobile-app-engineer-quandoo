import 'package:flutter/material.dart';
import 'package:quandoo/core/style/color.dart';
import 'package:quandoo/features/home/presentation/widgets/nav_bar_header.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:quandoo/features/merchant/presentation/view/merchant_listing_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int selectedIndex = 0;

  final screens = [
    const MerchantListingScreen(),
    const SizedBox(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Row(
        children: [
          NavigationRail(
            extended: true,
            leading: const NavBarHeader(),
            destinations:  [
              NavigationRailDestination(
                icon: const Icon(Icons.account_balance_outlined),
                label: Text("Merchants", style: TextStyle(fontSize: 32.sp),),
              ),
              NavigationRailDestination(
                icon: const Icon(Icons.eleven_mp,),
                label: Text("Other", style: TextStyle(fontSize: 32.sp)),
              ),
            ],
            onDestinationSelected: (index) {

            },
            selectedIconTheme: const IconThemeData(color: primaryColor),
            selectedLabelTextStyle: const TextStyle(color: primaryColor),
            selectedIndex: selectedIndex,
          ),
          Expanded(child: screens[selectedIndex]),
        ],
      ),
    ));
  }
}
