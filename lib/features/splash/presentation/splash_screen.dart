import 'package:flutter/material.dart';
import 'package:quandoo/core/constants/routes.dart';
import 'package:quandoo/core/constants/strings.dart';
import 'package:quandoo/core/style/color.dart';
import 'package:quandoo/gen/assets.gen.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    Future.delayed(const Duration(seconds: 1), (){
      print(homeScreen);
      Navigator.pushNamed(context, homeScreen);
    });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: primaryColor,
      body: SizedBox.expand(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Assets.images.quandoLogoMini.image(
              height: 200.h,
              width: 200.h,
            ),
            const Text(pleaseWait, style: TextStyle(color: whiteColor,),),
          ],
        ),
      ),
    );
  }
}
