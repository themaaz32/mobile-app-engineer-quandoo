import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:quandoo/core/constants/routes.dart';
import 'package:quandoo/core/models/all_merchant_usecase_param.dart';
import 'package:quandoo/features/merchant/data/data_sources/merchant_remote_data_source_implementation.dart';
import 'package:quandoo/features/merchant/data/repository_implementation/merchant_repository_implementation.dart';
import 'package:quandoo/features/merchant/domain/repository_contract/merchant_repository_contract.dart';
import 'package:quandoo/features/merchant/domain/usecase/get_all_merchants_use_case.dart';
import 'package:quandoo/features/merchant/domain/usecase/get_merchant_detail_use_case.dart';
import 'package:quandoo/features/merchant/presentation/bloc/merchant_bloc.dart';
import 'package:quandoo/features/merchant/presentation/bloc/merchant_bloc_event.dart';
import 'package:quandoo/features/merchant/presentation/view/merchant_listing_screen.dart';
import 'package:quandoo/features/splash/presentation/splash_screen.dart';
import 'core/data/remote_client.dart';

void main() {
  runApp(
    MultiRepositoryProvider(
      providers: [
        RepositoryProvider<MerchantRepository>(
          create: (BuildContext context) {
            return MerchantRepositoryImplementation(
              MerchantRemoteDataSourceImplementation(
                remoteClient: RemoteClientImplementation(),
              ),
            );
          },
        )
      ],
      child: BlocProvider<MerchantBloc>(
        create: (BuildContext context) {
          return MerchantBloc(
            getAllMerchantsUseCase:
                GetAllMerchantsUseCase(context.read<MerchantRepository>()),
            getMerchantDetailUseCase: GetMerchantDetailUseCase(
              context.read<MerchantRepository>(),
            ),
          );
        },
        child: const MyApp(),
      ),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);
    return ScreenUtilInit(
      designSize: const Size(1920, 1080),
      builder: () => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Quandoo App',
        theme: ThemeData(
          primarySwatch: Colors.amber,
        ),
        routes: routes,
        home: const SplashScreen(),
        builder: EasyLoading.init(),
      ),
    );
  }
}
