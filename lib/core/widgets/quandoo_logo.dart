import 'package:flutter/material.dart';
import 'package:quandoo/gen/assets.gen.dart';

class QuandooLogo extends StatelessWidget {
  const QuandooLogo({
    Key? key,
    required this.width,
    required this.height,
  }) : super(key: key);

  final double width;
  final double height;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: Assets.images.quandoLogoMini.image(),
    );
  }
}
