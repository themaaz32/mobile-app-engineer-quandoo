import 'package:flutter/material.dart';

const primaryColor = Color(0xfffab33d);
const whiteColor = Color(0xffffffff);
const greyTextColor = Color(0xffAAAAAA);