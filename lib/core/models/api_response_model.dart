import 'package:equatable/equatable.dart';
import 'package:quandoo/core/error/exceptions.dart';

class ApiResponse<T> extends Equatable{
  final bool isSuccess;
  final T? data;
  final ServerException? error;

  const ApiResponse({required this.isSuccess, this.data, this.error});

  @override
  // TODO: implement props
  List<Object?> get props => [data, error, isSuccess];

}