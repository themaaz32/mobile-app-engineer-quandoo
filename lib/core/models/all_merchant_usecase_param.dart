import 'package:equatable/equatable.dart';
import 'package:quandoo/core/constants/constants.dart';

class AllMerchantParam extends Equatable {
  const AllMerchantParam({
    this.query="",
    this.limit=merchantRecordsLimitConstant,
    this.offset,
  });

  final String query;
  final int? offset;
  final int limit;

  bool get isFirstPage => offset == 0;

  @override
  List<Object?> get props => [query, offset, limit];
}
