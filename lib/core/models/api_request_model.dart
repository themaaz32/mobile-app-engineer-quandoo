class ApiRequest{
  final String path;
  final Map<String, dynamic>?  query;

  ApiRequest({required this.path, this.query});
}