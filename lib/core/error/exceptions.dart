class ServerException {
  const ServerException({
    this.code,
    this.apiMessage,
    this.serverMessage,
    this.isResponseError,
  });

  final String? serverMessage;
  final String? apiMessage;
  final int? code;
  final bool? isResponseError;
}
