import 'package:quandoo/features/home/presentation/home_screen.dart';
import 'package:quandoo/features/merchant/presentation/view/merchant_detail_screen.dart';
import 'package:quandoo/features/splash/presentation/splash_screen.dart';

const splashScreen = "splash";
const homeScreen = "/home";
const merchantDetailScreen = "/merchant_detail";
const merchantListingScreen = "/merchants";

final routes = {
  splashScreen : (context) => const SplashScreen(),
  homeScreen : (context) => const HomeScreen(),
  merchantDetailScreen : (context) => const MerchantDetailScreen(),
};