const offsetConstant = "offset";
const limitConstant = "limit";
const queryConstant = "query";

const limitedRecordsCountConstant = 120;
const merchantRecordsLimitConstant = 20;

const noImageUrl = "https://lh3.googleusercontent.com/proxy/i-QMoTEc2UXBiSJatz3hMl1qaRuKscYhYFEahqGp49tTGTx3a-b-TOewwwK5P2hF9HYbojyfI_wY-NsHAbNILCGifz5AHyZOlg7kUNo8uNmz_CH0gxyKx9P-LLoqq7s";