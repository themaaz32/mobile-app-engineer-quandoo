import 'dart:async';

import 'package:dio/dio.dart';
import 'package:quandoo/core/error/exceptions.dart';
import 'package:quandoo/core/models/api_request_model.dart';

abstract class RemoteClient {
  Future<Response> get({required ApiRequest request});
}

class RemoteClientImplementation extends RemoteClient {
  late Dio _dio;

  RemoteClientImplementation() {
    _dio = Dio(BaseOptions(baseUrl: "https://api.quandoo.com"));
    addInterceptors();
  }

  @override
  Future<Response> get({required ApiRequest request}) async {
    final CancelToken _cancelToken = CancelToken();

    final timer =
        Timer(const Duration(seconds: 10), () => _cancelToken.cancel());

    try {
      final result = await _dio.get(
        request.path,
        queryParameters: request.query,
        cancelToken: _cancelToken,
      );

      timer.cancel();

      return result;
    } on DioError catch (e) {
      throw ServerException(
        code: e.response?.statusCode,
        serverMessage: e.message,
        apiMessage: e.response?.statusMessage,
        isResponseError: e.type == DioErrorType.response,
      );
    }
  }
  
  void addInterceptors(){
    _dio.interceptors.add(LogInterceptor(responseBody: false));
  }
}
