# Quandoo Implementation Documentation

[[_TOC_]]

## 1. Description

The Project mainly contains only two features that is paginated listing of the merchants and showing the detail of the merchant. The application specifically designed for Tablets; However it can also be functional on other mobile devices.


## 2. Application Architecture

The Architecture used to build the application is Clean Code Architecture proposed by Uncle Bob. Clean code Architecture is choosen to ensure application scalability and testability. Application is broken in to modular isolated layers, so that each layer can modularly perform its designated job. Dependency for the whole project only pointing inwards, so to say, only the outer layer can access and know about the inner layers.


## 3. Squence Flow Diagram

![Flow Diagram](https://user-images.githubusercontent.com/50517157/146081203-23169b22-90e8-4f41-8f0f-fc10df8df361.png)


## 4. State Management

Bloc is Implemented as state manager in this application. Bloc is very handy when it comes to readability as it based on real world scenarios, that is to say, converting an event into a new state with new data. Bloc is also quite stable and has great community support, and it has nice and smooth testing mechanism.


## 5. Testability

Application is mostly testable, with almost 60 percent of coverage; However, it can be obviously increased with time.

![Test](https://user-images.githubusercontent.com/50517157/146084502-05798174-6703-415e-90f1-673b04312d7e.png)


## 6. Building and runing the application.

Application is tested and run on ios tablets mainly; However, obviously it can also be run on android tablet.

Build with below command

```bash
  flutter run
```

Application Live Picture

![App Live](https://user-images.githubusercontent.com/50517157/146085479-bd0799bb-6459-4db2-94ae-96626944ce31.png)


## 7. Authors

- [Maaz Aftab](https://www.github.com/themaaz32)

